var webpackConfig = require("./webpack.test");

module.exports = function (config) {
    var _config = {
        basePath: "./",

        frameworks: [ "jasmine" ],

        files: [
            {pattern: "./karma-test-shim.js", watched: false}
        ],

        preprocessors: {
            "./karma-test-shim.js": [ "webpack", "sourcemap" ]
        },

        client:{
            clearContext: false // leave Jasmine Spec Runner output visible in browser
          },
              
        webpack: webpackConfig,

        webpackMiddleware: {
            stats: "errors-only"
        },

        webpackServer: {
            noInfo: true
        },

        reporters: [ "progress", "kjhtml" ],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: [ "Chrome" ],
        singleRun: false
    };

    config.set(_config);
};
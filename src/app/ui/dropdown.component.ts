
import {ChangeDetectorRef, Inject, HostListener, forwardRef, Component, OnInit, Input, Output, EventEmitter, ElementRef, AfterContentInit } from "@angular/core";
import {FormControl, NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, Validator } from '@angular/forms';

declare var module : any;
declare var $: any;

export class UifDropdownChange {
  public source: UifDropdown;
  public value: any;
}

export const UIF_DROPDOWN_CONTROL_VALUE_ACCESSOR : any = {
    provide: NG_VALUE_ACCESSOR, 
    useExisting: forwardRef(() => UifDropdown),
    multi: true
};

export const UIF_DROPDOWN_CONTROL_VALIDATORS : any = {
    provide: NG_VALIDATORS, 
    useExisting: forwardRef(() => UifDropdown),
    multi: true
};

@Component({
    moduleId: ''+module.id,
    selector: 'f-dropdown',
    providers: [UIF_DROPDOWN_CONTROL_VALUE_ACCESSOR, UIF_DROPDOWN_CONTROL_VALIDATORS],
    template: `<div (click)="dropdownClick()" (blur)="onBlur()" class='ms-Dropdown' [class.is-open]='isOpen' [class.is-disabled]='disabled' tabindex="0">
    <i class="ms-Dropdown-caretDown ms-Icon  ms-Icon--ChevronDown"></i>
    <span class="ms-Dropdown-title" [ngClass]="{'overlay': item == undefined }">{{label}}</span>
    <ul class="ms-Dropdown-items">
        <ng-content></ng-content>
    </ul>
</div>`,
    styles: ['.ms-Dropdown-title { white-space: nowrap; }', '.ms-Dropdown-items {overflow-y: auto}',
    '.ms-Dropdown {cursor: pointer}',
    '.overlay {color: lightgray;}',
    `
    .ms-Dropdown:hover .ms-Dropdown-title.overlay , 
    .ms-Dropdown:hover .ms-Dropdown-caretDown.overlay, 
    .ms-Dropdown:focus .ms-Dropdown-title.overlay, 
    .ms-Dropdown:focus .ms-Dropdown-caretDown.overlay, 
    .ms-Dropdown:active .ms-Dropdown-title.overlay, 
    .ms-Dropdown:active .ms-Dropdown-caretDown.overlay {
        color: lightgray;}
    `,
    ':host-context(.ms-TextField) .ms-Dropdown { font-size: 12px;}',
    '.ms-Icon--ChevronDown {top: auto !important;font-size: 14px; margin-bottom: 3px;}',
    `
        .ms-Dropdown-items:before{
            border: 0 !important;
        }
    `
    ]
})
export class UifDropdown  implements OnInit, AfterContentInit, ControlValueAccessor, Validator  {

    @Input() public item : any = null;
    @Input() public label : string;
    @Input() public required : boolean;
    @Input() public disabled : boolean = false;
    @Output() public itemChange = new EventEmitter();
    private isOpen: boolean = false;
    private onTouched: () => any = () => {};
    private controlValueAccessorChangeFn: (value: any) => void = (value) => {};
    private validateFn:any = () => {};

    constructor(@Inject(forwardRef(() => ElementRef)) private elementRef: ElementRef, @Inject(forwardRef(() => ChangeDetectorRef)) private changeDetector: ChangeDetectorRef) {
    }

    private onBlur(){
        this.onTouched();
    }

    public validate(c: FormControl) {
        return this.validateFn(c);
    }

    public ngOnInit() {

        var clicked = false;
        $(this.elementRef.nativeElement).click((e:any) => {
            clicked = true;
        });   
        $(document).click((e:any)=>{
            if(!clicked && this.isOpen === true)
            {
                this.isOpen = false;
                if(!this.destroyed){
                    this.changeDetector.detectChanges();
                }
            }
            clicked = false;
        });
    }

    private destroyed : boolean = false;
    public ngOnDestroy() {
        this.destroyed = true;
    }

    public setTitle(html: string)
    {
        $(this.elementRef.nativeElement).find('.ms-Dropdown-title').html(html);
    }

    public get value(): any { return this.item; };
  
    public set value(item: any) {
      if (item !== this.item) {
        this.item = item;
        this.controlValueAccessorChangeFn(item);

    	let event = new UifDropdownChange();
        event.source = this;
        event.value = this.item;
        this.itemChange.emit(event);

        if(!this.item){
            this.setTitle(this.label);
        }
      }
    }

    public dropdownClick(){
        if(!this.disabled){
            this.isOpen = !this.isOpen;
        }
    }

    public ngAfterContentInit() {
    }

    public writeValue(value: any) {
        this.value = value;
    }

    public registerOnChange(fn: (value: any) => void) {
        this.controlValueAccessorChangeFn = fn;
    }

    public registerOnTouched(fn: any) {
        this.onTouched = fn;
    }
}

@Component({
    moduleId: ''+module.id,
    selector: 'f-dropdown-option',
    template: '<li><ng-content></ng-content></li>',
    host: {'class': 'ms-Dropdown-item'},
    styles: [':host(.ms-Dropdown-item.is-selected, .ms-Dropdown-item.ms-Dropdown-item--selected) {line-height: 38px !important;}']
})
export class UifDropdownOption implements OnInit{

    @Input() public value : any;
    @Output() public valueChange = new EventEmitter();
    private subscription : any;

    constructor(@Inject(forwardRef(() => UifDropdown)) public dropdown : UifDropdown, @Inject(forwardRef(() => ElementRef)) private elementRef: ElementRef) {

    }

    @HostListener('click') public onClick() {
                this.selectItem();
    }

    public ngOnInit() {
        this.subscription = this.dropdown.itemChange.subscribe((event: UifDropdownChange) =>{
            if(this.equal(event.value, this.value))
            {
                this.selectItem();
            }        
        });
    }

    private equal(a,b)
    {
        if( (typeof a == 'object' && a != null) &&
            (typeof b == 'object' && b != null) )
        {
            var count = [0,0];
            var key: any;
            for( key in a) count[0]++;
            for( key in b) count[1]++;
            if( count[0]-count[1] != 0) {return false;}
            for( key in a)
            {
            if(!(key in b) || !this.equal(a[key],b[key])) {return false;}
            }
            for( key in b)
            {
            if(!(key in a) || !this.equal(b[key],a[key])) {return false;}
            }
            return true;
        }
        else
        {
            return a === b;
        }
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    public selectItem(){
        $(this.elementRef.nativeElement)
        .siblings('f-dropdown-option')
        .removeClass('is-selected');

        $(this.elementRef.nativeElement)
        .addClass('is-selected');

        this.dropdown.value = this.value;

        var html = $(this.elementRef.nativeElement).find('li').html();
        this.dropdown.setTitle(html);
    }
}


import {forwardRef, Inject, Component, OnInit, ElementRef} from '@angular/core';

import * as $ from 'jquery';
import * as fabric from 'office-ui-fabric/dist/js/jquery.fabric.js';
declare var module : any;

@Component({ 
    moduleId: ''+module.id,
    selector: 'spinner',
    styles: [':host { display: none; margin: auto; width: 50%; margin-top: 20px; }',
    '.ms-Spinner { width: 20px; margin: auto;}'
    ],
    template: '<div class="ms-Spinner"></div>'
})
export class Spinner implements OnInit{

    private spinner : any;

    constructor(@Inject(forwardRef(() => ElementRef)) private elementRef: ElementRef) {
    }

    public ngOnInit() {
        this.spinner = new fabric['Spinner']($('.ms-Spinner',$(this.elementRef.nativeElement))[0]);
    }

    public ngDoCheck() {
    }

    public stop(){
        $(this.elementRef.nativeElement).css('display', 'none');
    }

    public start(){
        $(this.elementRef.nativeElement).css('display', 'block');
    }
}
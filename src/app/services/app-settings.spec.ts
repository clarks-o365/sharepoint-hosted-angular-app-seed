import {AppSettings,ApiSettings} from './app-settings';


describe('AppSettings', () => {

    let sut : AppSettings;
  
    beforeEach(() => {
        sut = new AppSettings();
        sut.instrumentationKey = "ajsdfhjbasdfhjba sdfjha sdfjas dfja sdf";
        sut.api = <ApiSettings> { 
            damUrl : "https://api.com/api"
        };
    });
  
    describe('expected values', () => {
        it('dam api url', () => {
            expect(sut.api.damUrl).toBe("https://api.com/api");
        });
        it('instrumentationKey', () => {
            expect(sut.instrumentationKey).toBe("ajsdfhjbasdfhjba sdfjha sdfjas dfja sdf");
        });
    });
  });


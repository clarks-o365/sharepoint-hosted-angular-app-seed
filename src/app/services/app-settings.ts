export class AppSettings {
    
    public instrumentationKey: string;
    public api : ApiSettings;
}

export class ApiSettings{
    public searchUrl: string;
    public storageUrl: string;
    public searchQueryKey: string;
    public damUrl: string;
}


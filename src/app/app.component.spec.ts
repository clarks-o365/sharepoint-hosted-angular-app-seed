import {ComponentFixture, TestBed, async} from '@angular/core/testing';
import {Component,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {AppSettings} from '@app/services';


describe('AppComponent', () => {

  let fixture : ComponentFixture<AppComponent>;

  let declarations = [AppComponent];

  describe('when created', () => {
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: declarations,
        schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
        providers:[{ provide: AppSettings, use: {} }        ]
      }).compileComponents();
  
      fixture = TestBed.createComponent(AppComponent);
  
    }));
  
    it('should create the app', async(() => {
        var sut = fixture.componentInstance;
        expect(sut).toBeTruthy();
    }));

    it("should have expected title", async(() => {
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('h2').textContent).toContain('Product Selection');
    }));
      
  });
});

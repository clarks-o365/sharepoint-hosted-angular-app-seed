import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpModule, XHRBackend, RequestOptions} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {APP_BASE_HREF} from '@angular/common';

import {AppComponent} from './app.component';
import {AppSettings} from './services';
import {Spinner, UifDropdown, UifDropdownOption} from './ui';
import {ServicesModule}     from './services/services.module';

import * as URL from 'url-parse';
import * as $ from 'jquery';

const appId = "clarks-product-selection";

let appSettingsFactory = () : AppSettings => {
    return window['$'+appId+'-config'];
};

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        HttpClientModule,
        ServicesModule
    ],
    declarations: [
        AppComponent,
        UifDropdown, UifDropdownOption,
        Spinner
    ],
    providers: [
        {
            provide: APP_BASE_HREF, useValue: '/'
        },
        {provide: AppSettings, useFactory: appSettingsFactory}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

    static readonly configFile = appId+".config.js";
    static readonly authAppFile = "auth-app.aspx";
    static readonly hostPath = "/sites/assets/SiteAssets/";

    public static ready(callback: () => any){

        var params = new URLSearchParams(window.location.search);
        var hostUrl = params.get('SPHostUrl');
        var appUrl = params.get('SPAppWebUrl');

        var authAppUrl = new URL(hostUrl);
        authAppUrl.pathname = AppModule.hostPath + AppModule.authAppFile

        var authApp_iframe = $('<iframe />', { name: appId+'-sp-login', id: appId+'-sp-login', src: authAppUrl });
        var authApp_added:any = authApp_iframe.appendTo('body');
        
        authApp_added[0].onload = function(){
            console.info('loaded ' + authAppUrl);

            var configUrl = params.get('SPHostUrl') + '/SiteAssets/' + AppModule.configFile;
            $.getScript(configUrl, (result : any) =>{

                console.info('loaded ' + configUrl);
                var config = window['$'+appId+'-config'];

                var apiVersionUrl = config.api.damUrl + '/api/version'
                var api_iframe = $('<iframe />', { name: 'dam-api-login', id: 'dam-api-login', src: apiVersionUrl });
                var api_added:any = api_iframe.appendTo('body');
                api_added[0].onload = function(){

                    console.info('loaded ' + apiVersionUrl);
                    callback();
                };
            });
        }
    }
}

import { ViewChild, Component, OnInit } from '@angular/core';

import * as $ from 'jquery';

@Component({
    selector: 'product-selection-app',
    template: require('./app.component.html'),
    styles: [require('./app.component.css').toString(),
            ':host{ display: block;height: 100% }']
})

export class AppComponent  implements OnInit {

    
    constructor() {
    }
    
    public ngOnInit() {
    }
}
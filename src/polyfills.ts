import 'core-js/es6';
import 'core-js/es7/reflect';
import 'url-search-params-polyfill';
import 'whatwg-fetch';

(function(self)
{
    if(Headers.prototype['entries']){
        return;
    }
    
    var support = {
        searchParams: 'URLSearchParams' in self,
        iterable: 'Symbol' in self && 'iterator' in Symbol,
        blob: 'FileReader' in self && 'Blob' in self && (function() {
        try {
            new Blob()
            return true
        } catch(e) {
            return false
        }
        })(),
        formData: 'FormData' in self,
        arrayBuffer: 'ArrayBuffer' in self
    }

    function iteratorFor(items) {
        var iterator = {
        next: function() {
                var value = items.shift()
                return {done: value === undefined, value: value}
            }
        }

        if (support.iterable) {
        iterator[Symbol.iterator] = function() {
            return iterator
            }
        }

        return iterator
    }

    Headers.prototype['entries'] = function() {
        var items = []
        this.forEach(function(value, name) { items.push([name, value]) })
        return iteratorFor(items)
    }
})(typeof self !== 'undefined' ? self : this);

require('zone.js/dist/zone');
if (process.env.ENV === 'production') {
    // Production
} else {
    // Development and test
    Error['stackTraceLimit'] = Infinity;
    require('zone.js/dist/long-stack-trace-zone');
}
(function(global){
    
    var config = {
        instrumentationKey: 'c740498d-2c5b-4f1c-8f02-ac1c1e72a149',
        api: { 
            searchUrl: 'https://clarks-assets.search.windows.net/indexes/products/docs/search?api-version=2016-09-01',
            storageUrl: 'https://clarksdigitalassets.blob.core.windows.net',
            searchQueryKey: '50ED1B970C294EF3F419A6AB97A52021',
            damUrl: 'https://dam.clarks365.com'

        }
    };

    
        if("function" == typeof define){
            define([], function() { return config });
        }
        else{
            global['$clarks-product-selection-config'] = config;
        }
    })(window);
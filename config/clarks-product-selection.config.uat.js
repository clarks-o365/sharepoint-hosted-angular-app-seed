(function(global){
    
            var config = {
                instrumentationKey: 'd79ef29c-1569-4887-817f-233393831057',
                api: { 
                    searchUrl: 'https://clarks-assets-uat.search.windows.net/indexes/products/docs/search?api-version=2016-09-01',
                    storageUrl: 'https://clarksdigitalassetsuat.blob.core.windows.net',
                    searchQueryKey: '11C4ABFF7253F1A94275D6A2EC6EABC2',
                    damUrl: 'https://dam.clarks365uat.com'

                }
            };
    
        if("function" == typeof define){
            define([], function() { return config });
        }
        else{
            global['$clarks-product-selection-config'] = config;
        }
    })(window);
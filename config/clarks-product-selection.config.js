(function(global){
    
    var config = {
            instrumentationKey: '875d7b0e-a377-4de6-9bca-5c378f234e49',
            api: { 
                searchUrl: 'https://clarks-assets-dev.search.windows.net/indexes/products/docs/search?api-version=2016-09-01',
                storageUrl: 'https://clarksdigitalassetsdev.blob.core.windows.net',
                searchQueryKey: '2C7B1542CAF919B88382839A9915EFCB',
                damUrl: 'https://clarksdamapi-dev.azurewebsites.net'

            }
        };


    if("function" == typeof define){
        define([], function() { return config });
    }
    else{
        global['$clarks-product-selection-config'] = config;
    }
})(window);
/// <binding BeforeBuild='webpack:build-dev' />
var gulp = require("gulp");
var gutil = require("gulp-util");
var webpack = require("webpack");
var webpackConfig = require("./webpack.config.js");
var htmlreplace = require('gulp-html-replace');
var rev = require('gulp-rev-hash');
var htmlmin = require('gulp-htmlmin');
var args = require('yargs').argv;
var xmlpoke = require('xmlpoke');
var replace = require('gulp-replace');
var webpackConfig = require("./webpack.config.js");
var Karma = require('karma').Server;

gulp.task('webpack:build', ['enable-prod'], function (callback) {

	var config = Object.create(webpackConfig);
	config.plugins = config.plugins.concat(
		new webpack.optimize.UglifyJsPlugin()
	);

	// run webpack
	webpack(config, function(err, stats) {
		if(err) throw new gutil.PluginError("webpack:build", err);
		gutil.log("[webpack:build]", stats.toString({
			colors: true
		}));
		callback();
    });
});

gulp.task('enable-prod', function () {
   return gulp.src('src/environments/environment.ts')
        .pipe(replace('production: false','production: true'))
        .pipe(gulp.dest('src/environments/'));
});

gulp.task('prepare-aspx', function () {

   return gulp.src('Pages/Default.aspx')
        .pipe(htmlreplace({
            'appjs': ['../scripts/polyfills.bundle.js', '../scripts/vendor.bundle.js','../scripts/app.bundle.js'] }
            ))
        .pipe(gulp.dest('build1/'));
});

gulp.task('prepare-version', ['prepare-aspx'], function () {

    return gulp.src('build1/**/*.aspx')
        .pipe(rev({assetsDir: 'scripts'}))
        .pipe(gulp.dest('build2/'));
});

gulp.task('post-build', ['prepare-version', 'version-app-build'], function () {
    return gulp.src('build2/**/*.aspx')
        .pipe(htmlmin({
            collapseWhitespace: false,
            removeComments: true,
            caseSensitive: true,
            keepClosingSlash: true
        }))
        .pipe(gulp.dest('Pages/'));
});

gulp.task('version-app-build', function (cb) {

    xmlpoke('./AppManifest.xml',
        function (xml) {
            xml.addNamespace('n', 'http://schemas.microsoft.com/sharepoint/2012/app/manifest')
            .set('n:App/@Version', '1.0.0.' + (args.BUILD_NUMBER || '1'));
        });
    cb();
});

gulp.task('unit-test', function (done) {
    new Karma({
      configFile: __dirname + '/karma.conf.js',
      singleRun: true
    }, function (errorCode) { karmaCompleted(errorCode, done); }).start();
});

function karmaCompleted(errorCode, done) {
    if (errorCode !== 0) {
        done();
        return process.exit(errorCode);
    } else {
        done();
    }
}

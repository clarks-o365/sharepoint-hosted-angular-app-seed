const path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

 devtool: 'source-map',
 devServer: {
   contentBase: './Scripts',
   hot: false,
  inline: false
 },
 entry: {
    'polyfills' : './src/polyfills.ts',
    'vendor': './src/vendor.ts',
    'app' : './src/main.ts' 
  },
 module: {
   rules: [
     {
      test: require.resolve(path.resolve(__dirname, 'node_modules/office-ui-fabric/dist/js/jquery.fabric.js')),
      use :'exports-loader?fabric'
    },

     {
       test: /\.ts?$/,
       use: 'awesome-typescript-loader',
       exclude: /node_modules/
     },
    {
        test: /\.html$/,
        use: 'html-loader'
    },
     {
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader'
         ]
       }
   ]
 },
 resolve: {
   extensions: [".tsx", ".ts", ".js"],
   alias: {
      "@services": path.resolve(__dirname, 'src/app/services'), 
      "@app": path.resolve(__dirname, 'src/app/'), 
      jquery_fabric : path.resolve(__dirname, 'node_modules/office-ui-fabric/dist/js/jquery.fabric.js'),
      pickaDate : path.resolve(__dirname, 'node_modules/office-ui-fabric/dist/lib/PickaDate.js')
    }
 },
 output: {
   filename: '[name].bundle.js',
   path: path.resolve(__dirname, 'Scripts')
 },
 plugins: [
      // Workaround for angular/angular#11580
      new webpack.ContextReplacementPlugin(
          // The (\\|\/) piece accounts for path separators in *nix and Windows
          /\@angular(\\|\/)core(\\|\/)esm5/,
          './src', // location of your src
          {} // a map of your routes
      ),

      new webpack.optimize.CommonsChunkPlugin({
          name: ['app', 'vendor', 'polyfills']
      }),
      new ExtractTextPlugin('[name].css'),
      new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          "window.jQuery": "jquery"
      })            
  ]
};

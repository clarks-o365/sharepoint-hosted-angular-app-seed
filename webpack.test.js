const path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

 devtool: 'inline-source-map',

 resolve: {
  extensions: [".ts", ".js"],
  alias: {
    "@services": path.resolve(__dirname, 'src/app/services'), 
    "@app": path.resolve(__dirname, 'src/app/'), 
    jquery_fabric : path.resolve(__dirname, 'node_modules/office-ui-fabric/dist/js/jquery.fabric.js'),
    pickaDate : path.resolve(__dirname, 'node_modules/office-ui-fabric/dist/lib/PickaDate.js')
   }
},
module: {
   rules: [
     {
      test: require.resolve(path.resolve(__dirname, 'node_modules/office-ui-fabric/dist/js/jquery.fabric.js')),
      use :'exports-loader?fabric'
    },

     {
       test: /\.ts?$/,
       use: 'awesome-typescript-loader',
       exclude: /node_modules/
     },
    {
        test: /\.html$/,
        use: 'html-loader'
    },
     {
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader'
         ]
       }
   ]
 },
 plugins: [
      // Workaround for angular/angular#11580
      new webpack.ContextReplacementPlugin(
          // The (\\|\/) piece accounts for path separators in *nix and Windows
          /angular(\\|\/)core(\\|\/)@angular/,
          './src', // location of your src
          {} // a map of your routes
      ),
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
    })            
  ]
};
